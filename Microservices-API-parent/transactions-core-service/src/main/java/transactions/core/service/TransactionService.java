package transactions.core.service;

import java.util.List;

import transactions.core.model.Transaction;

public interface TransactionService {

	public Transaction getTransactionDetail(long transactionId);
	
	public List<Transaction> getTransactions(String accountId);
}
