## Overview
This is an api service for an account. It exposes REST endpoints that are used to provide account functionality. It implements a circuit breaker pattern, which stops the client from waiting needlessly for an unresponsive accounts-composite-service. 

Below is the code snippet of AccountServiceImpl that uses [Netflix Hystrix] (https://github.com/Netflix/Hystrix/wiki/How-it-Works) to implement circuit breaker pattern.

```
@HystrixCommand(fallbackMethod = "getFallbackAccounts", commandProperties = {
	@HystrixProperty(name = "execution.isolation.strategy", value = "THREAD"),
	@HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "10"),
	@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "1000") })
@Override
public AccountCompositeCollection getAccountComposites(String customerId) {
   .....
}

```

## Pre-requisites
Projects that need to be started before

* configuration-service - For pulling the configuration information
* discovery-service - For starting the Eureka server to register all the services (api, core and composite)

## Running the application
* Build the application by running the command: mvn clean install
* Run the application by running the command: java -jar build/libs/accounts-api-service-0.0.1-SNAPSHOT.jar

Invoke the service by using the below urls
* http://localhost:8082/accounts

## External Configuration
The project derives it's configuration from the configuration-service. We have defined the spring.cloud.config.uri in the bootstrap.yml file and that tells the application where to pick up the external configurations. In our case, the URL points to the running configuration-service (http://localhost:8888). 


Important dependencies in classpath
* spring-cloud-config-client dependency so that the application can consume the config server
* spring-cloud-starter-eureka dependency to register the service in discovery server 
* spring-cloud-starter-hystrix dependency to enable the circuit breaker for the service
