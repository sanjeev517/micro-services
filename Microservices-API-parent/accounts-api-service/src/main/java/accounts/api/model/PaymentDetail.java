package accounts.api.model;


public class PaymentDetail {
	
	private double amountDue;
	private double interestDue;
	private String dueDate;

	public PaymentDetail(){}
	
	public PaymentDetail(double amountDue, double interestDue, String dueDate){
		this.amountDue = amountDue;
		this.interestDue = interestDue;
		this.dueDate = dueDate;
	}
	
	public double getAmountDue() {
		return amountDue;
	}
	public void setAmountDue(double amountDue) {
		this.amountDue = amountDue;
	}
	public double getInterestDue() {
		return interestDue;
	}
	public void setInterestDue(double interestDue) {
		this.interestDue = interestDue;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	
}
