package accounts.api.service;

import java.util.List;

import accounts.api.model.Account;
import accounts.api.model.AccountComposite;
import accounts.api.model.AccountCompositeCollection;


public interface AccountService {
	
	public List<Account> getAccounts(String customerId);
	
	public Account getAccount(String accountNumber);
	
	public AccountCompositeCollection getAccountComposites(String customerId); 
	
	public AccountComposite getAccountComposite(String accountNumber);

}
