package payments.core.service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import payments.core.model.PaymentDetail;
import payments.core.model.PaymentDetailCollection;

@Service
public class PaymentsServiceImpl implements PaymentsService {

	@Override
	public PaymentDetailCollection getPaymentDetails(String accountNumber) {
		
		PaymentDetailCollection paymentDetail = new PaymentDetailCollection();
		paymentDetail.setAccountNumber(accountNumber);
		paymentDetail.setLastPaymentAmount(100);
		paymentDetail.setLastPaymentDate(new Date());
		
		paymentDetail.setPaymentDetails(this.getPaymentDetailsAsList());
		
		return paymentDetail;
	}
	
	private List<PaymentDetail> getPaymentDetailsAsList(){
		//Calendar calendar = GregorianCalendar.getInstance();
		
		List<PaymentDetail> paymentDetails = Arrays.asList(
			new PaymentDetail(10, 20, "01/01/2017"),
			new PaymentDetail(11, 22, "01/01/2017"),
			new PaymentDetail(12, 23, "01/01/2017"));
		
		return paymentDetails;
	}
}
