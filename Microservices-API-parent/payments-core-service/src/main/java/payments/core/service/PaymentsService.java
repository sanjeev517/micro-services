package payments.core.service;

import payments.core.model.PaymentDetailCollection;

public interface PaymentsService { 
	
	public PaymentDetailCollection getPaymentDetails(String accountNumber);
	
}
