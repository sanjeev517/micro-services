## Api Microservices
This is an operational model of Microservices Reference Architecture. It showcases vertical partitioning of microservices in to layers like:
* Core Microservices: Responsible for persistence of business data and applying business rules and other logic
* Composite Microservices: They can either orchestrate core services to perform a business process or aggregate information from a number of core services.
* API Microservices: Responsible for exposing functionality externally

This project also showcases the usage of patterns such as circuit breaker, service registry and discovery, central configuration, monitoring dashboard using backing services from [Pilot-Microservices] (Pilot-Microservices). This project will help the team understand best practices for Microservices Architecture and get them started with the same in a very short time.

## Table of Contents
* [Application Architecture](#application-architecture)
* [Architecture Components](#architecture-components)

## <a name="application-architecture"></a> Application Architecture
Architecture consists of 6 business services and backing services from Pilot-Microservices such as [Configuration Service] (/Pilot-Microservices/blob/master/configuration-service/README.md), [Discovery Service] (/Pilot-Microservices/blob/master/discovery-service/README.md) and [Dashboard Service] (/Pilot-Microservices/blob/master/dashboard-service/README.md). Each component is built separately using their own build file. 

![Microservices Architecture](Janus-ApiMicroservices.png)

## <a name="architecture-components"></a> Architecture Components
* Core services responsible for handling information regarding accounts, payments and transactions. Account Core Service manages accounts, Payments Core Service manages payments for a given account and Transactions Core Service manages transactions for a given account.
    * [Accounts Core Service] (accounts-core-service/README.md) - Accounts Core Service manages Account
    * [Payments Core Service] (payments-core-service/README.md) - Payments Core Service for a given account
    * [Transactions Core Service] (transactions-core-service/README.md) - Transactions Core Service for a given account

* Composite service, accounts-composite-service, that aggregates information from the two core services and composes a view of account information together with payment information for an account.
    * [Accounts Composite Service] (accounts-composite-service/README.md) - Composite Service that aggregates the results of accounts and payment details for a given account

* API services, Accounts API Service retrieves account and payment information by invoking Accounts Composite Service and Transactions API Service retrieves transactions from Transactions Core Service.  
    * [Account Api Aervice] (accounts-api-service/README.md) - API service for accounts
    * [Transactions Api Service] (transactions-api-service/README.md) - API service for transactions

## Using the Application

#### Running on local machine
* You can build the projects by maven. 
    * Maven: Run maven at the parent project "Api-Microservices", this will build all the individual projects. Run the  individual project jar by running the command: mvn spring-boot:run

* You can run the applications in the order listed below.
    * Configuration Service - This application should be run first as it holds properties for all applications 
    * Discovery Service - This application should be run second as all the services register themselves with discovery server
    * All other services (Core, Composite and API) and other backing services such as Dashboard Service

* Please refer to the individual readme files on instructions of how to run the services. 