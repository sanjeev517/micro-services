package transactions.api.service;

import transactions.api.model.Transaction;
import transactions.api.model.TransactionCollectionResponse;

public interface TransactionService {

	public Transaction getTransactionDetail(String accountNumber, long transactionId);
	
	public TransactionCollectionResponse getTransactions(String accountId);
}
