package accounts.composite.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import accounts.composite.model.Account;
import accounts.composite.model.AccountCollectionResponse;
import feign.Feign;
import feign.httpclient.ApacheHttpClient;

@Service
public class AccountsCoreServiceClient {
	
	private static final Logger LOG = LoggerFactory.getLogger(AccountsCoreServiceClient.class);
	AccountsCoreServiceApi accountsCoreServiceApi;
	
	
	public AccountsCoreServiceClient(){
		accountsCoreServiceApi = Feign.builder().client(new ApacheHttpClient()).target(AccountsCoreServiceApi.class,
				 "http://localhost:8086");
	}

	public Account getAccount(String accountNumber) {
		LOG.info("Calling core service with FeignClient");
		Account account = accountsCoreServiceApi.getAccountDetail("123");
		return account;
	}

	public List<Account> getAccounts(String customerId) {
		LOG.info("Calling core service with FeignClient for all Accounts");
		AccountCollectionResponse accountCollectionResponse = accountsCoreServiceApi.getAccounts(customerId);
		return accountCollectionResponse.getAccounts();
	}
	

}
