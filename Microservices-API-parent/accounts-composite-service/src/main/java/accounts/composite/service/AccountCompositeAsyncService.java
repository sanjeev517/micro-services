package accounts.composite.service;

import java.util.List;
import java.util.concurrent.ExecutionException;

import accounts.composite.model.AccountComposite;

public interface AccountCompositeAsyncService {
		
		public AccountComposite getAccount(String accountNumber) throws ExecutionException, 
		InterruptedException;
		
		public List<AccountComposite> getAccounts(String customerId) throws ExecutionException, InterruptedException;
}
