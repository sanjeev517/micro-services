package accounts.composite.service;

import accounts.composite.model.PaymentDetailCollection;
import feign.Param;
import feign.RequestLine;

public interface PaymentsCoreServiceApi {
	
	@RequestLine("GET /accounts/{accountNumber}/payment-details")
	public PaymentDetailCollection getPaymentDetails(@Param("accountNumber") String accountNumber);
	
	
}
