package accounts.composite.service;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import accounts.composite.model.Account;
import accounts.composite.model.AccountComposite;
import accounts.composite.model.PaymentDetailCollection;

@Service
public class AccountCompositeAsyncServiceImpl implements AccountCompositeAsyncService {
	
	private static final Logger LOG = LoggerFactory.getLogger(AccountCompositeAsyncServiceImpl.class);
	
	@Autowired
	private AccountsCoreServiceClient accoutServiceClient;
	
	@Autowired
	private PaymentsCoreServiceClient paymentServiceClient;
	
	// Executor service runs with up to 6 worker threads simultaneously
	// When thread pool is exhausted, up to 4 tasks will be queued -
	// additional tasks are rejected triggering the PushbackHandler
	final int POOL_SIZE = 6;
	final int QUEUE_SIZE = 4;
	
	// Set up a thread pool executor with a bounded queue and a PushbackHandler
	ExecutorService executor =
			new ThreadPoolExecutor(POOL_SIZE, POOL_SIZE, // Core pool size, max pool size
			0, TimeUnit.SECONDS, // Timeout for unused threads
			new ArrayBlockingQueue<Runnable>(QUEUE_SIZE),
			new PushbackHandler());

	@Override
	public AccountComposite getAccount(String accountNumber) throws ExecutionException, 
		InterruptedException {
		LOG.info("Getting Account Details for Account:"+accountNumber);
		
		CompletableFuture<AccountComposite> accountCompositeFuture = 
				
				CompletableFuture.supplyAsync(() -> {
					Account account = accoutServiceClient.getAccount(accountNumber);
					AccountComposite accComposite = new AccountComposite();
					accComposite.setAccountNumber(account.getAccountNumber());
					accComposite.setNickName(account.getNickName());
					return accComposite;
					
				}, executor).thenCompose((AccountComposite accCompositeInstance) -> 
					CompletableFuture.supplyAsync(() -> {
						PaymentDetailCollection paymentDetails = paymentServiceClient.getPaymentDetails(accCompositeInstance.getAccountNumber());
						accCompositeInstance.setPaymentDetails(paymentDetails.getPaymentDetails());
						accCompositeInstance.setLastPaymentAmount(paymentDetails.getLastPaymentAmount());
						accCompositeInstance.setLastPaymentDate(paymentDetails.getLastPaymentDate());
						
					return accCompositeInstance;
				}, executor));
		
		return accountCompositeFuture.get();
	}

	@Override
	public List<AccountComposite> getAccounts(String customerId) throws ExecutionException, InterruptedException{
		
		LOG.info("Getting Accounts for customer:"+customerId);
		
		CompletableFuture<List<Account>> accountsCollFuture = CompletableFuture.supplyAsync(() -> {
			List<Account> accounts = accoutServiceClient.getAccounts(customerId);
			return accounts;
		}, executor);
		
		List<AccountComposite> accountComposites = accountsCollFuture.get().stream().map(
				new Function<Account, AccountComposite>(){

					@Override
					public AccountComposite apply(Account account) {
						// retrieve payments for an Account
						PaymentDetailCollection paymentDetails = paymentServiceClient.getPaymentDetails(account.getAccountNumber());
						AccountComposite accountComposite = new AccountComposite();
						accountComposite.setAccountNumber(account.getAccountNumber());
						accountComposite.setNickName(account.getNickName());
						accountComposite.setPaymentDetails(paymentDetails.getPaymentDetails());
						return accountComposite;
					}
				}
				).collect(Collectors.toList());
		
		return accountComposites;
	}

}
