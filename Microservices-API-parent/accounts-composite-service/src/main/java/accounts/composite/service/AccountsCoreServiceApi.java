package accounts.composite.service;

import accounts.composite.model.Account;
import accounts.composite.model.AccountCollectionResponse;
import feign.Param;
import feign.RequestLine;

public interface AccountsCoreServiceApi {
	
	@RequestLine("GET /accounts/{accountNumber}")
	public Account getAccountDetail(@Param("accountNumber") String accountNumber);
	
	@RequestLine("GET /accounts")
    public AccountCollectionResponse getAccounts(
    		@Param(value="customerNumber") String customerNumber) ;

}
