package accounts.composite.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import accounts.composite.model.PaymentDetailCollection;
import feign.Feign;
import feign.httpclient.ApacheHttpClient;


@Service
public class PaymentsCoreServiceClient {
	
	private static final Logger LOG = LoggerFactory.getLogger(PaymentsCoreServiceClient.class);
	private PaymentsCoreServiceApi paymentsCoreServiceApi;
	
	public PaymentsCoreServiceClient(){
		paymentsCoreServiceApi = Feign.builder().client(new ApacheHttpClient()).target(PaymentsCoreServiceApi.class,
				 "http://localhost:8085");
	}
	
	public PaymentDetailCollection getPaymentDetails(String accountNumber){
//		PaymentDetailCollection paymentDetailColl = new PaymentDetailCollection();
//		PaymentDetail paymentDetail = new PaymentDetail(12, 12, new Date());
//		paymentDetailColl.addPaymentDetail(paymentDetail);
//		return paymentDetailColl;
		
		LOG.info("Calling core service with FeignClient");
		PaymentDetailCollection payments = paymentsCoreServiceApi.getPaymentDetails(accountNumber);
		return payments;
	}

}
