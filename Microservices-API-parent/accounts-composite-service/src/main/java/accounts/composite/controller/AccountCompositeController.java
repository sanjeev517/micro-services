package accounts.composite.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import accounts.composite.model.AccountComposite;
import accounts.composite.model.AccountCompositeCollection;
import accounts.composite.service.AccountCompositeAsyncService;
import accounts.composite.service.AccountCompositeService;

@RestController
public class AccountCompositeController {
	private static final Logger LOG = LoggerFactory.getLogger(AccountCompositeController.class);
	
	@Autowired
	private AccountCompositeService accoutService;
	
	@Autowired
	private AccountCompositeAsyncService accoutAsyncService;

    @RequestMapping(value="/accounts", method = RequestMethod.GET, headers = "Accept=application/json")
    public AccountCompositeCollection getAccounts() {
    	
    	AccountCompositeCollection response = new AccountCompositeCollection();
    	response.setAccounts(accoutService.getAccounts("123"));
    	return response;
    }
    
    @RequestMapping("/accounts/{accountNumber}")
    public AccountComposite getAccountDetail(
    		@PathVariable("accountNumber") String accountNumber) {
        return accoutService.getAccount(accountNumber);
    }
    
    @RequestMapping("/accounts-async/{accountNumber}")
    public AccountComposite getAccountDetailAsync(
    		@PathVariable("accountNumber") String accountNumber) throws Exception{
    	
    	LOG.info("getAccountDetailAsync called");
        return accoutAsyncService.getAccount(accountNumber);
    }
    
    @RequestMapping(value="/accounts-async", method = RequestMethod.GET, headers = "Accept=application/json")
    public AccountCompositeCollection getAccountsAsync() throws Exception {
    	
    	AccountCompositeCollection response = new AccountCompositeCollection();
    	response.setAccounts(accoutAsyncService.getAccounts("123"));
    	return response;
    }
}
