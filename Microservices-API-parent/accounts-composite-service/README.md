## Overview
This is a composite service for an account and payment details. It exposes REST endpoints that are used to provide account and payment functionality.

## Pre-requisites
Projects that need to be started before
* configuration-service - For pulling the configuration information
* discovery-service - For starting the Eureka server to register all the services (api, core and composite)

## Running the application
* Build the application by running the command: mvn clean install
* Run the application by running the command: java -jar build/libs/accounts-composite-service-0.0.1-SNAPSHOT.jar

Invoke the service by using the below urls
* http://localhost:8084/accounts/123
* http://localhost:8084/accounts/

## External Configuration
The project derives it's configuration from the configuration-service. We have defined the spring.cloud.config.uri in the bootstrap.yml file and that tells the application where to pick up the external configurations. In our case, the URL points to the running configuration-service (http://localhost:8888). 

Important dependencies in classpath
* spring-cloud-config-client dependency so that the application can consume the config server
* spring-cloud-starter-eureka dependency to register the service in discovery server 
* spring-cloud-starter-hystrix dependency to enable the circuit breaker for the service

