## Overview
The figure below illustrates how Configuration Service works. 

![Configuration Servive] (Configuration-Service.png)

* The Git repository is used to store the configuration since Git is good at tracking and storing changes.
* The Configuration Service keeps itself up to date with the Git repository, and serves HTTP requests with configurations for the clients.
* The clients request the configuration from the Configuration Service, which sets the properties in the application.

###### Client Side Usage
The microservices have a spring.application.name property configured in bootstrap.yml or bootstrap.properties file. This property is used to retrieve the configuration from the Configuration Service.

```
spring:
  application:
    name: accounts-api-service
  cloud:
    config:
      enabled: true
      uri: http://<configuration-service host>:<Port>
```

## Pre-requisites
None

### Projects that need to be started before
This is the first application that needs to run since it pulls the configuration information (like what port to run etc) that is needed by rest of the applications to start.

### Running the application
* Build the application by running the "mvn clean install" maven command at the "configuration-service" project root folder on the terminal.
* Run application by running command: java -jar target/configuration-service-0.0.1-SNAPSHOT.jar 
