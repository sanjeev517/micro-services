## Overview
Microservice-based application typically runs in a virtualized or containerized environments where the number of instances of a service and their locations changes dynamically. We need a mechanism that enables the clients of service make requests to a dynamically changing set of service instances.

The following diagram shows the structure of service registry pattern.
![Discovery Servive] (ServiceRegistry.png)

This application uses Netflix OSS [Eureka] (https://github.com/Netflix/eureka/wiki/Eureka-at-a-glance) to maintain a registry of service information and enable service clients to discover each other. When a service registers with Discovery Service, it provides meta-data about itself such as host and port, health indicator URL, home page etc. Discovery Service receives heartbeat messages from each instance belonging to a service. If the heartbeat fails over a configurable timetable, the instance is normally removed from the registry.

###### Client Side Usage
The @EnableDiscoveryClient activates the Netflix Eureka DiscoveryClient implementation. The eureka client enabled microservice is registered under the name specified in the spring.application.name property. It uses eureka.client.serviceUrl property to locate the Discovery Service.

```
spring:
  application:
    name: accounts-api-service
eureka:
  client:
    registerWithEureka: true
    serviceUrl:
      defaultZone: http://localhost:8761/eureka/
```      

## Projects that need to be started before

* [configuration-service] (../configuration-service/README.md) - For pulling the configuration information

## Running the application

* Build the application by running the "mvn clean install" maven command at the "discovery-service" project root folder on the terminal.
* Run application by running command: java -jar target/discovery-service-0.0.1-SNAPSHOT.jar 

###### External Configuration
The application derives it's external configuration from the [configuration-service] (../configuration-service/README.md) service. We have defined the spring.cloud.config.uri in the bootstrap.yml file and that tells the application where to pick up the external configurations. In our case, the URL points to the running [configuration-service] (../configuration-service/README.md). You also need to have the spring-cloud-config-client dependency in the classpath so that the application can consume the config server.

```
spring:
  application:
    name: discovery-service
  cloud:
    config:
      uri: http://localhost:8888
```
