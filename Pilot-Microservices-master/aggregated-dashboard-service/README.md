## Overview
The aggregated-dashboard-service monitors a large number of circuit breakers in a system landscape. It uses [Netflix Turbine] (https://github.com/Netflix/Turbine/wiki) to aggregate data from multiple servers into a single stream of metrics supporting the dashboard application, which in turn streams the aggregated data to the browser for display in the UI.

###### <br/>
###### Sample screenshot for aggregated dashboard for circuit breaker enabled services:

![Aggregated Dashboard] (AggregatedDashboard.png)

## Pre-requisites

### Projects that need to be started before
* configuration-service - For pulling the configuration information
* discovery-service - For pulling the configuration information
* dashboard-service - 

## Running the application

* Build the application by running the "mvn clean install" maven command at the "aggregated-dashboard-service" project root folder on the terminal.
* Run application by running command: java -jar target/aggregated-dashboard-service-0.0.1-SNAPSHOT.jar 
* Open the dashboard-service url http://localhost:7979/hystrix in the browser.
* Add http://localhost:10000/turbine.stream in the textfield and click "Monitor Stream" button to monitor all the circuit breakers in a system landscape.

## External Configuration
The application derives it's external configuration from the [configuration-service] (../configuration-service/README.md) service. 

