## Overview
The gateway service acts as a dynamic and active reverse proxy. It loads service discovery components and routes the requests to an appropriate service. It uses [Netflix Zuul] (https://github.com/Netflix/zuul/wiki) to act as a gatekeeper to the outside world and for an entry point to the microservices in the system landscape.

## Pre-requisites

### Projects that need to be started before
* configuration-service - For pulling the configuration information
* discovery-service - For starting Eureka server and pulling all registered services


## Running the application

* Build the application by running the "mvn clean install" maven command at the "gateway-service" project root folder on the terminal.
* Run application by running command: java -jar target/gateway-service-0.0.1-SNAPSHOT.jar 

## External Configuration
The application derives it's external configuration from the [configuration-service] (../configuration-service/README.md) service. 

