## Overview
Dashboard Service is used to provide a graphical overview of the health of each circuit breaker enabled applications.  

## Running the application

* Build the application by running the "mvn clean install" maven command at the "dashboard-service" project root folder on the terminal.
* Run application by running command: java -jar target/dashboard-service-0.0.1-SNAPSHOT.jar  

###### <br/>
Run this app as a normal Spring Boot application and then go to the home page in a browser (http://localhost:7979/hystrix). If you run from this project it will be on port 7979 (per the application.yml). On the home page is a form where you can enter the URL for an event stream to monitor, for example:  

* the circuit breaker enabled microservice running locally on port 8082: http://localhost:8082/hystrix.stream  

###### <br/>
###### Sample screenshot for Hystrix Dashboard for circuit breaker enabled service:

![Hystrix Dashboard for Accounts API Service](https://cloud.githubusercontent.com/assets/5256077/12741287/f3751954-c9a0-11e5-8595-f9e0f83e06ac.jpg)