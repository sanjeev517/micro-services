## Pilot-Microservices
Pilot-Microservices is a reference implementation of Microservices Architecture. It provides extensible backing services to support Microservices based Architectures and demonstrates end-to-end best practices for building a Microservice Architecture using Spring Cloud and Netflix open-source projects.

### Microservices Reference Architecture
Looking at the reference architecture diagram, there are following backing services. These backing services solve a set of concerns that are purely operational and security-related. Each of these is a Spring Boot applications and are considered to be backing services to the Microservices based application. The Backing service is a twelve-factor methodology term, which premises that the third-party service dependencies should be treated as attached resources to the cloud native applications.

* [Discovery Service] (#discovery-service)
* [Configuration Server] (#configuration-service)
* [Dashboard Service] (#dashboard-service)
* [Gateway Service] (#gateway-service)
* [Aggregated Dashboard Service] (#aggregated-dashboard-service)

![Microservices Architecture](Pilot-Microservices-v1.png)

#### <a name="discovery-service"></a> Discovery Service
Discovery Service is a Spring Cloud application and is responsible for maintaining a registry of service information. Each microservice will subscribe to a Discovery Service at start-up and provide its networking information, which includes its network address. By doing this, all other applications in the environment will be able to locate other subscribers by downloading a service registry and caching it locally. Please refer [Discovery Service] (discovery-service/README.md) for additional details.

#### <a name="configuration-service"></a>Configuration Service 
Configuration Service is a Spring Cloud application that centralizes external configurations of all applications. It allows applications to retrieve their tailored configurations for the target environment. It is configured to use git repository and pulls configuration from a git repository "service-configurations". Please refer [Configuration Service] (configuration-service/README.md) for additional details.

#### <a name="dashboard-service"></a>Dashboard Service 
Dashboard Service is a Spring Cloud application that provides a graphical overview of the health of each circuit breaker enabled applications. Please refer [Dashboard Service] (dashboard-service/README.md) for additional details.

#### <a name="gateway-service"></a>Gateway Service 
Gateway Service is a Spring Cloud application and provides single entry point for all clients. It is responsible for securely exposing HTTP routes from backing microservices as a single unified REST API. It matches a request URL fragment from a front-end application to a back-end microservice through a reverse proxy to retrieve the remote REST API response. Please refer [Gateway Service] (gateway-service/README.md) for additional details.

#### <a name="aggregated-dashboard-service"></a>Aggregated Dashboard Service 
The service provides the dashboard with information from all circuit breakers in a system landscape. Please refer [Aggregated Dashboard Service] (aggregated-dashboard-service/README.md) for additional details.

## Using the backing services

#### Running on local machine
* You can build the projects by running maven.  
    * Maven: Run maven at the parent project "Pilot-Microservices", this will build all the individual projects. Run the  individual project jar by running the command: mvn spring-boot:run

* You can run the applications in the order listed below.
    * Configuration Service - This application should be run first as it holds properties for all applications 
    * Discovery Service - This application should be run second as all the services register themselves with discovery service
    * All other backend services

* Please refer to the individual readme files on instructions of how to run the services. 

* Please refer to [API Microservices] (/Janus-ApiMicroservices) and upcoming Reactive Microservices that demonstrate the usage of these backend services with microservices.

## Upcoming enhancements
* Auth server to avoid unauthorized access from the outside to internal microservices.
* Enhance dashboard service for aggregated view of all circuit breaker enabled applications
* Dockerize the backend services
